#include "widget.h"
#include <QApplication>
#include <QVBoxLayout>
#include "viewer.h"
#include <QWidget>
#include <QObject>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget qw;
    QVBoxLayout *layout = new QVBoxLayout();
    qw.setLayout(layout);

    Administrator *admin = new Administrator(); //модель
    Viewer *v = new Viewer(admin);
    //Controller *c = new Controller(admin, v);

    layout->addWidget(v);
    //layout->addWidget(c);
    qw.show();
    //Widget w;
    //w.show();

    return a.exec();
}
