#ifndef VIEWER_H
#define VIEWER_H
#include "widget.h"
#include <QTextEdit>
#include <QVBoxLayout>


class Viewer : public QWidget
{
public:
    Viewer(Administrator *admin){
        _admin = admin;
        _text = new QTextEdit("QQQ");
        QVBoxLayout *layout = new QVBoxLayout();
        setLayout(layout);
        layout->addWidget(_text);
    }

private:
    Administrator *_admin;
    QTextEdit *_text;
};

#endif // VIEWER_H
