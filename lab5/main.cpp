#include <QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlRecord>

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("C:\\Users\\Asus\\Desktop\\NECHTO\\lab5\\addressbook.txt");
    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError().text();
        return false;
    }
    return true;
}
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if (!createConnection()) {
        return -1;
    }
    // Создаем базу
    QSqlQuery query;
    QString str = "CREATE TABLE addressbook (number AUTO_INCREMENT, name VARCHAR(15), phone VARCHAR(12), email VARCHAR(15));";
    if (!query.exec(str)) {
     qDebug() << "Unable to create a table";
     }

    // Добавляем данные в базу
    QString strF =
            "INSERT INTO addressbook (number, name, phone, email) "
            "VALUES(%1, '%2', '%3', '%4');";
    str = strF.arg("1")
            .arg("Gosha")
            .arg("+79851429647")
            .arg("gosha@gmail.com");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("2")
            .arg("Jess")
            .arg("+79851429647")
            .arg("jess@gmail.com");

    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("3")
            .arg("Sanya")
            .arg("+79491429141")
            .arg("sanyaaaa@gmail.com");

    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    if (!query.exec("SELECT * FROM addressbook;")) {
        qDebug() << "Unable to execute query — exiting";
        return 1;
    }
    // Считываем данные из базы
    QSqlRecord rec = query.record();
    int nNumber = 0;
    QString strName;
    QString strPhone;
    QString strEmail;
    while (query.next()) {
        nNumber = query.value(rec.indexOf("number")).toInt();
        strName = query.value(rec.indexOf("name")).toString();
        strPhone = query.value(rec.indexOf("phone")).toString();
        strEmail = query.value(rec.indexOf("email")).toString();
        qDebug() << nNumber << " " << strName << ";\t"
                 << strPhone << ";\t" << strEmail;


    }

    str = "SELECT COUNT(*) FROM addressbook WHERE name LIKE 'G%'";
    if (!query.exec(str)) {
    qDebug() << "Unable to make insert operation";
    }
    query.first();

    qDebug() << query.value(0).toInt();

    query.exec("drop table addressbook");
    return 0;

    //return a.exec();
}
