#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include <iostream>
#include <fstream>
#include "windowadmin.h"
#include "windowuser.h"
#include "windowworker.h"
#include "database.h"
#include <QPalette>


using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), //инициализация параметров родительского класса
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    admin = new WindowAdmin(this);
    user = new WindowUser(this);
    reg = new registration(this);
    worker = new WindowWorker(this);
    setWindowTitle("Вход");

    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
    qApp->setStyleSheet("QPushButton { color: #1f0f21; background-color: #f0c4f2; }"
                        "QMessageBox {background-color: #fae0fc }"
                        "QComboBox {background-color: #f0c4f2 }"
                       /* "QHeaderView::section { background-color: #fae0fc }" */
                        "QTableWidget {border-color: #fae0fc }"
                        "QLineEdit {border-color: #fae0fc }");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textEdited(const QString &arg1)
{
    login = arg1;
}

void MainWindow::on_lineEdit_2_textEdited(const QString &arg1)
{
    pass = arg1;
}

void MainWindow::on_pushButton_clicked()
{
    //login = ui->lineEdit->text();
    //pass = ui->lineEdit_2->text();

    DataBaseLogPass lp;
    DataBase db;

    bool check = false;


    for(int id = 0; id < lp.GetVectorSize(); id++){

        lp.SetId(id);

        if (QString::fromStdString(lp.GetDataById("aothorization")) == "admin" && pass == QString::fromStdString(lp.GetDataById("pass")) && login == QString::fromStdString(lp.GetDataById("log"))) {
            DataBaseAdmin a;
            a.SetAothorization("admin");
            this->close(); //закрываем текущее окошко
            QMessageBox::information(this, "Вход", "Вы успешно вошли в систему в качестве администратора.");
            check = true;
            admin->show();
            break;
        }

        if (QString::fromStdString(lp.GetDataById("aothorization")) == "worker" && pass == QString::fromStdString(lp.GetDataById("pass")) && login == QString::fromStdString(lp.GetDataById("log"))) {
            DataBaseWorker w;
            w.SetAothorization("worker");
            this->close(); //закрываем текущее окошко
            QMessageBox::information(this, "Вход", "Вы успешно вошли в систему в качестве работника.");
            check = true;
            worker->show();
            break;
        }

        if (QString::fromStdString(lp.GetDataById("aothorization")) == "user" && pass == QString::fromStdString(lp.GetDataById("pass")) && login == QString::fromStdString(lp.GetDataById("log"))) {
            DataBaseUser u;
            u.SetAothorization("user");
            this->close(); //закрываем текущее окошко
            QMessageBox::information(this, "Вход", "Вы успешно вошли в систему в качестве пользователя.");
            check = true;
            user->show();
            break;
        }
    }

     if (check == false){ //если ни одна строчка не подошла, выполняем условие
           QMessageBox::warning(this,"Ошибка","Вы не вошли в систему. Попробуйте ещё раз.");
           ui->lineEdit->clear(); //стираем содержимое
           ui->lineEdit_2->clear(); //обоих полей ввода
     }
}


void MainWindow::on_pushButton_2_clicked()
{
    this->hide();
    reg->show();
}
