#include "jornal.h"
#include "ui_jornal.h"
#include "database.h"
#include "windowadmin.h"
#include "windowuser.h"
#include "windowworker.h"
#include <QMessageBox>
#include <QSignalMapper>
#include <QRadioButton>

Jornal::Jornal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Jornal)
{
    ui->setupUi(this);
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
    DataBaseJournal j;

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//отмена редактирования ВСЕХ полей
    setWindowTitle("Мой Журнал Заказов");
    ui->tableWidget->setRowCount(j.GetConstSize());
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Дата заказа" << "Время заказа" << "Блюдо" << "Цена" << "Вес (г.)");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch); //чтобы растянуть все столбцы
    //ui->tableWidget->horizontalHeader()->setStretchLastSection(true); //Чтобы растянуть последний столбец
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(n, QHeaderView::Stretch); //Чтобы растянуть столбец #n



    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromLocal8Bit(j.GetData("dish", i).c_str()));
        QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromStdString(j.GetData("price", i)));
        QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromStdString(j.GetData("weight", i)));
        QTableWidgetItem *itm4 = new QTableWidgetItem(QString::fromStdString(j.GetData("date", i)));
        QTableWidgetItem *itm5 = new QTableWidgetItem(QString::fromStdString(j.GetData("time", i)));
        ui->tableWidget->setItem(i, 0, itm4);
        ui->tableWidget->setItem(i, 1, itm5);
        ui->tableWidget->setItem(i, 2, itm1);
        ui->tableWidget->setItem(i, 3, itm2);
        ui->tableWidget->setItem(i, 4, itm3);
    }
}

Jornal::~Jornal()
{
    delete ui;
}

void Jornal::on_pushButton_clicked()
{
    this->close();
    DataBaseJournal j;
    if (j.GetAothorization() == "user"){
        WindowUser *u = new WindowUser(this);
        u->show();
    }
    if (j.GetAothorization() == "worker"){
        WindowWorker *w = new WindowWorker(this);
        w->show();
    }
    if (j.GetAothorization() == "admin"){
        WindowAdmin *a = new WindowAdmin(this);
        a->show();
    }
}


