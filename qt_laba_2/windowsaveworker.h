#ifndef WINDOWSAVEWORKER_H
#define WINDOWSAVEWORKER_H

#include <QDialog>

namespace Ui {
class WindowSaveWorker;
}

class WindowSaveWorker : public QDialog
{
    Q_OBJECT

public:
    explicit WindowSaveWorker(QWidget *parent = nullptr);
    ~WindowSaveWorker();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::WindowSaveWorker *ui;
};

#endif // WINDOWSAVEWORKER_H
