#ifndef WINDOWSHOWWORKER_H
#define WINDOWSHOWWORKER_H

#include <QDialog>

namespace Ui {
class WindowShowWorker;
}

class WindowShowWorker : public QDialog
{
    Q_OBJECT

public:
    explicit WindowShowWorker(QWidget *parent = nullptr);
    ~WindowShowWorker();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::WindowShowWorker *ui;
};

#endif // WINDOWSHOWWORKER_H
