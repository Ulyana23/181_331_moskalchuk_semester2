#ifndef SAVEMENU_H
#define SAVEMENU_H

#include <QDialog>

namespace Ui {
class SaveMenu;
}

class SaveMenu : public QDialog
{
    Q_OBJECT

public:
    explicit SaveMenu(QWidget *parent = nullptr);
    ~SaveMenu();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::SaveMenu *ui;
};

#endif // SAVEMENU_H
