#ifndef JORNAL_H
#define JORNAL_H

#include <QDialog>

namespace Ui {
class Jornal;
}

class Jornal : public QDialog
{
    Q_OBJECT

public:
    explicit Jornal(QWidget *parent = nullptr);
    ~Jornal();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Jornal *ui;
};

#endif // JORNAL_H
