#ifndef WINDOWPERSONALAREA_H
#define WINDOWPERSONALAREA_H

#include <QDialog>

namespace Ui {
class WindowPersonalArea;
}

class WindowPersonalArea : public QDialog
{
    Q_OBJECT

public:
    explicit WindowPersonalArea(QWidget *parent = nullptr);
    ~WindowPersonalArea();

private slots:
    void on_pushButton_2_clicked();

    
    void on_pushButton_clicked();

private:
    Ui::WindowPersonalArea *ui;
};

#endif // WINDOWPERSONALAREA_H
