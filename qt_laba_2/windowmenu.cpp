#include "windowmenu.h"
#include "ui_windowmenu.h"
#include "mainwindow.h"
#include "windowuser.h"
#include <QString>
#include "database.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QTime>
#include <QDate>

WindowMenu::WindowMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowMenu)
{
    ui->setupUi(this);
    setWindowTitle("Меню");
    //ui->TimeLabel->setText(currTime.toString("hh:mm:ss"));

    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);

    ui->comboBox->addItem("Блюдо");
    ui->comboBox->addItem("Категория");
    ui->comboBox->addItem("Цена");
    ui->comboBox->addItem("Вес");

    DataBaseMenu m;

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//отмена редактирования ВСЕХ полей
    ui->tableWidget->setRowCount(m.GetMenuSize());
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Добавить" << "Блюдо" << "Категория" << "Цена" << "Вес (г.)");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch); //чтобы растянуть все столбцы
    //ui->tableWidget->horizontalHeader()->setStretchLastSection(true); //Чтобы растянуть последний столбец
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(n, QHeaderView::Stretch); //Чтобы растянуть столбец #n



    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        QTableWidgetItem *checkBoxItem = new QTableWidgetItem();
        checkBoxItem->setCheckState(Qt::Unchecked);

        QTableWidgetItem *itm = new QTableWidgetItem(QString::fromLocal8Bit(m.GetMenuElem("dish", i).c_str()));
        QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromLocal8Bit(m.GetMenuElem("category", i).c_str()));
        QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromStdString(m.GetMenuElem("price", i)));
        QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromStdString(m.GetMenuElem("weight", i)));
            ui->tableWidget->setItem(i, 0, checkBoxItem);
            ui->tableWidget->setItem(i, 1, itm);
            ui->tableWidget->setItem(i, 2, itm1);
            ui->tableWidget->setItem(i, 3, itm2);
            ui->tableWidget->setItem(i, 4, itm3);

        }
        //ui->tableWidget->horizontalHeader()->setResizeContentsPrecision(0);
}



WindowMenu::~WindowMenu()
{
    delete ui;
}

void WindowMenu::on_pushButton_2_clicked()
{
    this->close();
    WindowUser *u = new WindowUser;
    u->show();
}

void WindowMenu::on_pushButton_3_clicked()
{
    DataBaseMenu m;

    ui->tableWidget->clear();
    string name = ui->lineEdit->text().toLocal8Bit().constData();
    ui->lineEdit->clear();

    string something;

    if(ui->comboBox->currentText() == "Категория"){
        ui->tableWidget->setRowCount(m.GetConstSize("category", name));
        something = "category";
    }
    if(ui->comboBox->currentText() == "Блюдо"){
        ui->tableWidget->setRowCount(m.GetConstSize("dish", name));
        something = "dish";
    }
    if(ui->comboBox->currentText() == "Цена"){
        ui->tableWidget->setRowCount(m.GetConstSize("price", name));
        something = "price";
    }
    if(ui->comboBox->currentText() == "Вес"){
        ui->tableWidget->setRowCount(m.GetConstSize("weight", name));
        something = "weight";
    }

    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Добавить" << "Блюдо" << "Категория" << "Цена" << "Вес (г.)");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        QTableWidgetItem *checkBoxItem = new QTableWidgetItem();
        checkBoxItem->setCheckState(Qt::Unchecked);

        QTableWidgetItem *itm = new QTableWidgetItem(QString::fromLocal8Bit(m.Search("dish", something, i, name).c_str()));
        QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromLocal8Bit(m.Search("category", something, i, name).c_str()));
        QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromStdString(m.Search("price", something, i, name)));
        QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromStdString(m.Search("weight", something, i, name)));
            ui->tableWidget->setItem(i, 0, checkBoxItem);
            ui->tableWidget->setItem(i, 1, itm);
            ui->tableWidget->setItem(i, 2, itm1);
            ui->tableWidget->setItem(i, 3, itm2);
            ui->tableWidget->setItem(i, 4, itm3);

        }
}

void WindowMenu::on_pushButton_4_clicked()
{
    ui->tableWidget->sortByColumn(2, Qt::SortOrder::AscendingOrder); //сортировка по алфавиту
}

void WindowMenu::on_pushButton_clicked()
{
    QTime currTime = QTime::currentTime();
    QDate currDate = QDate::currentDate();
    QString checkboxDish = "nothing";
    QString checkboxPrice = "nothing";
    QString checkboxWeight = "nothing";
    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
        if(ui->tableWidget->item(i, 0)->checkState()){

            checkboxDish = ui->tableWidget->item(i, 1)->text();
            checkboxPrice = ui->tableWidget->item(i, 3)->text();
            checkboxWeight = ui->tableWidget->item(i, 4)->text();

            QTableWidgetItem *checkBoxItem = new QTableWidgetItem();
            checkBoxItem->setCheckState(Qt::Unchecked);
            ui->tableWidget->setItem(i, 0, checkBoxItem);

            QFile journal("/Users/Asus/Desktop/NECHTO/qt_laba_2/journal.txt");
            if (journal.open(QIODevice::Append)){

                QTextStream journalS(&journal);

                ifstream j("/Users/Asus/Desktop/NECHTO/qt_laba_2/journal.txt");
                string str;
                int i = 0;

                while(getline(j, str)) i++;
                j.close();

                DataBaseUser u;
                if(i == 0) journalS << QString::fromLocal8Bit(u.GetId().c_str()) << ':' << currTime.toString("hh.mm.ss") << ':' << currDate.toString("dd/MM/yyyy") << ':' << QString::fromLocal8Bit(u.GetData("name").c_str()) << ':' << checkboxDish << ':' << checkboxPrice << ':' << checkboxWeight;
                else
                journalS << "\r\n" << QString::fromLocal8Bit(u.GetId().c_str()) << ':' << currTime.toString("hh.mm.ss") << ':' << currDate.toString("dd/MM/yyyy") << ':' << QString::fromLocal8Bit(u.GetData("name").c_str()) << ':' << checkboxDish << ':' << checkboxPrice << ':' << checkboxWeight;
                journal.close();
            }
        }
    }
    if(checkboxDish == "nothing") QMessageBox::warning(this, "Ошибка", "Вы ничего не заказали");
    else QMessageBox::information(this, "Ваш заказ", "Поздравляем, вы сделали свой заказ!");
}
