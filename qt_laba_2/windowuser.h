#ifndef WINDOWUSER_H
#define WINDOWUSER_H

#include <QDialog>
#include <windowmenu.h>

namespace Ui {
class WindowUser;
}

class WindowUser : public QDialog
{
    Q_OBJECT

public:
    explicit WindowUser(QWidget *parent = nullptr);
    ~WindowUser();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::WindowUser *ui;
    WindowMenu * wmenu;
};

#endif // WINDOWUSER_H
