#include "windowworker.h"
#include "ui_windowworker.h"
#include "workerpersonalarea.h"
#include "workerjournal.h"
#include "mainwindow.h"

WindowWorker::WindowWorker(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowWorker)
{
    ui->setupUi(this);
    setWindowTitle("Главное Меню");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
}

WindowWorker::~WindowWorker()
{
    delete ui;
}

void WindowWorker::on_pushButton_clicked()
{
    this->close();
    WorkerPersonalArea *pa = new WorkerPersonalArea(this);
    pa->show();
}

void WindowWorker::on_pushButton_2_clicked()
{
    this->close();
    WorkerJournal *j = new WorkerJournal(this);
    j->show();
}

void WindowWorker::on_pushButton_5_clicked()
{
    this->close();
    MainWindow *w = new MainWindow(this);
    w->show();
}
