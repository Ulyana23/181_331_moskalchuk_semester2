#include "windowuser.h"
#include "ui_windowuser.h"
#include "windowmenu.h"
#include "database.h"
#include "windowpersonalarea.h"
#include <QDebug>
#include "jornal.h"
#include "mainwindow.h"

WindowUser::WindowUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowUser)
{
    ui->setupUi(this);
    wmenu = new WindowMenu(this);
    setWindowTitle("Главное Меню");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
}

WindowUser::~WindowUser()
{
    delete ui;
}

void WindowUser::on_pushButton_clicked()
{
    this->close();
    wmenu->show();
}

void WindowUser::on_pushButton_3_clicked()
{
    this->close();
    WindowPersonalArea *pa = new WindowPersonalArea(this);
    pa->show();
}

void WindowUser::on_pushButton_2_clicked()
{
    this->close();
    Jornal *j = new Jornal(this);
    j->show();
}

void WindowUser::on_pushButton_5_clicked()
{
    this->hide();
    MainWindow *w = new MainWindow(this);
    w->show();
}
