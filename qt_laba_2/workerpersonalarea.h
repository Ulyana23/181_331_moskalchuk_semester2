#ifndef WORKERPERSONALAREA_H
#define WORKERPERSONALAREA_H

#include <QDialog>

namespace Ui {
class WorkerPersonalArea;
}

class WorkerPersonalArea : public QDialog
{
    Q_OBJECT

public:
    explicit WorkerPersonalArea(QWidget *parent = nullptr);
    ~WorkerPersonalArea();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::WorkerPersonalArea *ui;
};

#endif // WORKERPERSONALAREA_H
