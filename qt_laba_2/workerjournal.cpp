#include "workerjournal.h"
#include "ui_workerjournal.h"
#include "database.h"
#include "windowworker.h"
#include "windowuser.h"
#include "windowadmin.h"
#include <qradiobutton.h>
#include <qmessagebox.h>

WorkerJournal::WorkerJournal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WorkerJournal)
{
    ui->setupUi(this);
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
    DataBaseJournal j;

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//отмена редактирования ВСЕХ полей
    setWindowTitle("Журнал Заказов");
    if(j.GetElem("name", 0) == "") ui->tableWidget->setRowCount(0);
    else
    ui->tableWidget->setRowCount(j.GetVectorSize());
    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Заказ" << "Дата заказа" << "Время заказа" << "Имя" << "Блюдо" << "Цена" << "Вес (г.)");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch); //чтобы растянуть все столбцы
    //ui->tableWidget->horizontalHeader()->setStretchLastSection(true); //Чтобы растянуть последний столбец
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(n, QHeaderView::Stretch); //Чтобы растянуть столбец #n

    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        QTableWidgetItem *checkBoxItem = new QTableWidgetItem();
        checkBoxItem->setCheckState(Qt::Unchecked);

        QTableWidgetItem *itm = new QTableWidgetItem(QString::fromLocal8Bit(j.GetElem("name", i).c_str()));
        QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromLocal8Bit(j.GetElem("dish", i).c_str()));
        QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromStdString(j.GetElem("price", i)));
        QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromStdString(j.GetElem("weight", i)));
        QTableWidgetItem *itm4 = new QTableWidgetItem(QString::fromStdString(j.GetElem("date", i)));
        QTableWidgetItem *itm5 = new QTableWidgetItem(QString::fromStdString(j.GetElem("time", i)));
        ui->tableWidget->setItem(i, 1, itm4);
        ui->tableWidget->setItem(i, 2, itm5);
        ui->tableWidget->setItem(i, 0, checkBoxItem);
        ui->tableWidget->setItem(i, 3, itm);
        ui->tableWidget->setItem(i, 4, itm1);
        ui->tableWidget->setItem(i, 5, itm2);
        ui->tableWidget->setItem(i, 6, itm3);
    }
    if(j.GetElem("name", 0) == "") QMessageBox::information(this, "Журнал заказов", "Журнал заказов пуст");
}

WorkerJournal::~WorkerJournal()
{
    delete ui;
}

void WorkerJournal::on_pushButton_clicked()
{
    this->close();
    DataBaseJournal j;
    if (j.GetAothorization() == "user"){
        WindowUser *u = new WindowUser(this);
        u->show();
    }
    if (j.GetAothorization() == "worker"){
        WindowWorker *w = new WindowWorker(this);
        w->show();
    }
    if (j.GetAothorization() == "admin"){
        WindowAdmin *a = new WindowAdmin(this);
        a->show();
    }
}

void WorkerJournal::on_pushButton_2_clicked()
{
    bool flag = false;
    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
        if(ui->tableWidget->item(i, 0)->checkState()){
            flag = true;
            DataBaseJournal j;
            j.DeleteDish(i);
            j.VectorToFile();
            j.VectorClear();
        }
    }
        ui->tableWidget->clear();
        DataBaseJournal u;
        if(u.GetElem("name", 0) == "") ui->tableWidget->setRowCount(0);
        else
        ui->tableWidget->setRowCount(u.GetVectorSize());
        ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Заказ" << "Дата заказа" << "Время заказа" << "Имя" << "Блюдо" << "Цена" << "Вес (г.)");
        for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

            QTableWidgetItem *checkBoxItem = new QTableWidgetItem();
            checkBoxItem->setCheckState(Qt::Unchecked);

            QTableWidgetItem *itm = new QTableWidgetItem(QString::fromLocal8Bit(u.GetElem("name", i).c_str()));
            QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromLocal8Bit(u.GetElem("dish", i).c_str()));
            QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromStdString(u.GetElem("price", i)));
            QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromStdString(u.GetElem("weight", i)));
            QTableWidgetItem *itm4 = new QTableWidgetItem(QString::fromStdString(u.GetElem("date", i)));
            QTableWidgetItem *itm5 = new QTableWidgetItem(QString::fromStdString(u.GetElem("time", i)));
                ui->tableWidget->setItem(i, 1, itm4);
                ui->tableWidget->setItem(i, 2, itm5);
                ui->tableWidget->setItem(i, 0, checkBoxItem);
                ui->tableWidget->setItem(i, 3, itm);
                ui->tableWidget->setItem(i, 4, itm1);
                ui->tableWidget->setItem(i, 5, itm2);
                ui->tableWidget->setItem(i, 6, itm3);
        }

        if(flag == true) QMessageBox::information(this, "Заказ", "Заказ успешно доставлен");
        else QMessageBox::warning(this, "Журнал заказов", "Вам нечего доставлять");

        if(u.GetElem("name", 0) == "") QMessageBox::information(this, "Журнал заказов", "Журнал заказов пуст");
}
