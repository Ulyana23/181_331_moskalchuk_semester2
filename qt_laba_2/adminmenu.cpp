#include "adminmenu.h"
#include "ui_adminmenu.h"
#include "windowadmin.h"
#include "database.h"
#include "savemenu.h"
#include <QMessageBox>

AdminMenu::AdminMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdminMenu)
{
    ui->setupUi(this);
    setWindowTitle("Меню");

    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
    ui->pushButton->setStyleSheet("background-color: #f0c4f2");
    ui->pushButton_2->setStyleSheet("background-color: #f0c4f2");
    ui->pushButton_3->setStyleSheet("background-color: #f0c4f2");

    DataBaseMenu m;

    //ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//отмена редактирования ВСЕХ полей
    ui->tableWidget->setRowCount(m.GetMenuSize());
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Блюдо" << "Категория" << "Цена" << "Вес (г.)");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch); //чтобы растянуть все столбцы
    //ui->tableWidget->horizontalHeader()->setStretchLastSection(true); //Чтобы растянуть последний столбец
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(n, QHeaderView::Stretch); //Чтобы растянуть столбец #n


    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {


        QTableWidgetItem *itm = new QTableWidgetItem(QString::fromLocal8Bit(m.GetMenuElem("dish", i).c_str()));
        QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromLocal8Bit(m.GetMenuElem("category", i).c_str()));
        QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromStdString(m.GetMenuElem("price", i)));
        QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromStdString(m.GetMenuElem("weight", i)));
            ui->tableWidget->setItem(i, 0, itm);
            ui->tableWidget->setItem(i, 1, itm3);
            ui->tableWidget->setItem(i, 2, itm1);
            ui->tableWidget->setItem(i, 3, itm2);

        }

}

AdminMenu::~AdminMenu()
{
    delete ui;
}

void AdminMenu::on_pushButton_2_clicked()
{
    this->close();
    WindowAdmin *a = new WindowAdmin(this);
    a->show();
}

void AdminMenu::on_pushButton_clicked()
{
    this->close();
    SaveMenu *m = new SaveMenu(this);
    m->show();
}

void AdminMenu::on_pushButton_3_clicked()
{
    bool flag = false;

    string Dish;
    string Price;
    string Weight;
    string Category;
    DataBaseMenu m;

    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        Dish = ui->tableWidget->item(i, 0)->text().toLocal8Bit().constData();
        Category = ui->tableWidget->item(i, 1)->text().toLocal8Bit().constData();
        Price = ui->tableWidget->item(i, 2)->text().toLocal8Bit().constData();
        Weight = ui->tableWidget->item(i, 3)->text().toLocal8Bit().constData();

        if(m.GetMenuElem("dish", i) != Dish || m.GetMenuElem("category", i) != Category || m.GetMenuElem("price", i) != Price || m.GetMenuElem("weight", i) != Weight){
            m.SetMenu("dish", i, Dish);
            m.SetMenu("price", i, Price);
            m.SetMenu("weight", i, Weight);
            m.SetMenu("category", i, Category);
            flag = true;
        }
    }
    if(flag == true){
        m.VectorToFile();
        QMessageBox::information(this, "Сохранение", "Изменения успешно сохранены");
    }
    else QMessageBox::warning(this, "Ошибка", "Вы не произвели никаких изменений");
}
