#include "windowsaveworker.h"
#include "ui_windowsaveworker.h"
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <fstream>
#include "windowshowworker.h"
#include <QMessageBox>
#include "database.h"

using namespace std;

WindowSaveWorker::WindowSaveWorker(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowSaveWorker)
{
    ui->setupUi(this);
    setWindowTitle("Добавить Сотрудника");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);

    for(int i = 1; i < 32; i++){
        QString j = QString::number(i);
        if(i < 10)
            ui->day->addItem("0" + j);
        else
            ui->day->addItem(j);
    }

    for(int i = 1960; i < 2010; i++){
        QString j = QString::number(i);
        ui->year->addItem(j);
    }

    for(int i = 1; i < 13; i++){
        QString j = QString::number(i);
        if(i < 10)
            ui->month->addItem("0" + j);
        else
        ui->month->addItem(j);
    }
}

WindowSaveWorker::~WindowSaveWorker()
{
    delete ui;
}

void WindowSaveWorker::on_pushButton_clicked()
{
    DataBaseLogPass lp;

    QString log = ui->log->text();
    QString pass = ui->pass->text();
    QString name_1 = ui->name1->text();
    QString name_2 = ui->name2->text();
    QString name_3 = ui->name3->text();
    QString e_mail = ui->e_mail->text();
    QString phone = ui->phone->text();
    QString position = ui->position->text();
    QString year;
    QString month;
    QString day;

    for(int i = 1; i < 32; i++){
        QString j = QString::number(i);
        if(i < 10){
        if(ui->day->currentText() == '0'+j) day = '0'+j;
        }
        else if(ui->day->currentText() == j) day = j;
    }

    for(int i = 1960; i < 2010; i++){
        QString j = QString::number(i);
        if(ui->year->currentText() == j) year = j;
    }

    for(int i = 1; i < 13; i++){
        QString j = QString::number(i);
        if(i < 10){
        if(ui->month->currentText() == '0'+j) month = '0'+j;
        }
        else if(ui->month->currentText() == j) month = j;
    }

    bool flag;
    int x = phone.toInt(&flag, 10);
    int n = 1;
    while ((x/=10) > 0) n++;
    if(n != 9) flag = false;
    if(flag == false){
        QMessageBox::warning(this, "Ошибка", "Введён некоректный номер телефона");
    }

    QString date = (day + "/" + month + "/" + year);

    QString name = (name_1 + " " + name_2 + " " + name_3);

    int id = 0;

    ifstream test("/Users/Asus/Desktop/NECHTO/qt_laba_2/lab2.txt", ios::in);
    string line;

    while(getline(test, line)){
        id++;
    }

    test.close();

    bool check = false;

    if (log.size() != 0 && pass.size() != 0 && name_1.size() != 0 && name_2.size() != 0 && e_mail.size() != 0 && phone.size() != 0)
    {
        check = true;
    }

    for(int i = 0; i < lp.GetVectorSize(); i++){
        if(log == QString::fromStdString(lp.GetData("log", i))){
            QMessageBox::warning(this, "Ошибка", "Такой логин уже существует");
            check = false;
        }
    }
    if(check == true && flag == true){
        QFile reg("/Users/Asus/Desktop/NECHTO/qt_laba_2/lab2.txt");
        QFile file("/Users/Asus/Desktop/NECHTO/qt_laba_2/worker.txt");
        if (reg.open(QIODevice::Append) && file.open(QIODevice::Append)){

            QTextStream regS(&reg);
            QTextStream fileS(&file);

            regS << "\r\n" << id << ':' << log << ':' << pass << ":worker";
            fileS << "\r\n" << id << ':' << name << ':' << position << ':' << e_mail << ':' << "89" + phone << ':' << date;
            file.close();
            reg.close();
            QMessageBox::information(this, "Добавление сотрудника", "Новый сотрудник успешно добавлен");
            this->close();
            WindowShowWorker *wshw = new WindowShowWorker(this);
            wshw->show();
        }
    }
    else QMessageBox::warning(this, "Ошибка", "Вы допустили ошибку");
}

void WindowSaveWorker::on_pushButton_2_clicked()
{
    this->close();
    WindowShowWorker *wshw = new WindowShowWorker(this);
    wshw->show();
}
