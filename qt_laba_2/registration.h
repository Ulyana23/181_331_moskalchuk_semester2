#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include <string>
#include "windowadmin.h"
#include "windowuser.h"



using namespace std;
namespace Ui {
class registration;
}

class registration : public QDialog
{
    Q_OBJECT

public:
    explicit registration(QWidget *parent = nullptr);
    ~registration();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::registration *ui;
    QString log;
    QString pass;
    QString replay_pass;
    QString name_1;
    QString name_2;
    QString name_3;
    QString e_mail;
    QString day;
    QString month;
    QString year;
    QString phone;
    QString date;
    QString name;

//    string _log;
//    string _pass;
//    string _name;
//    string _e_mail;
//    string _date;
//    string _phone;

};

#endif // REGISTRATION_H
