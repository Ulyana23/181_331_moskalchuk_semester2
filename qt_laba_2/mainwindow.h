#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <string>
#include <iostream>
#include <fstream>
#include "windowadmin.h"
#include "windowuser.h"
#include "registration.h"
#include "windowworker.h"

using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void Read(QString line);


private slots:
    void on_lineEdit_textEdited(const QString &arg1);
    
    void on_lineEdit_2_textEdited(const QString &arg1);
    
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QString login;
    QString pass;
    string _login;
    string _pass;
    WindowAdmin *admin;
    WindowUser *user;
    registration *reg;
    WindowWorker *worker;
};




#endif // MAINWINDOW_H
