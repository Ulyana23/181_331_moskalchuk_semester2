#include "registration.h"
#include "ui_registration.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <iostream>
#include <fstream>
#include <string>
#include <QDebug>
#include "database.h"


using namespace std;

registration::registration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registration)
{
    ui->setupUi(this);
    setWindowTitle("Регистрация");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);

    for(int i = 1; i < 32; i++){
        QString j = QString::number(i);
        if(i < 10)
            ui->num->addItem("0" + j);
        else
            ui->num->addItem(j);
    }

    for(int i = 1960; i < 2010; i++){
        QString j = QString::number(i);
        ui->year->addItem(j);
    }

    for(int i = 1; i < 13; i++){
        QString j = QString::number(i);
        if(i < 10)
            ui->month->addItem("0" + j);
        else
        ui->month->addItem(j);
    }

}

registration::~registration()
{
    delete ui;
}

void registration::on_pushButton_clicked()
{
    DataBaseLogPass lp;

    log = ui->lineEdit->text();
    pass = ui->lineEdit_2->text();
    replay_pass = ui->lineEdit_3->text();
    name_1 = ui->lineEdit_4->text();
    name_2 = ui->lineEdit_5->text();
    name_3 = ui->lineEdit_11->text();
    e_mail = ui->lineEdit_6->text();
    phone = ui->lineEdit_10->text();

    for(int i = 1; i < 32; i++){
        QString j = QString::number(i);
        if(i < 10){
        if(ui->num->currentText() == '0'+j) day = '0'+j;
        }
        else if(ui->num->currentText() == j) day = j;
    }

    for(int i = 1960; i < 2010; i++){
        QString j = QString::number(i);
        if(ui->year->currentText() == j) year = j;
    }

    for(int i = 1; i < 13; i++){
        QString j = QString::number(i);
        if(i < 10){
        if(ui->month->currentText() == '0'+j) month = '0'+j;
        }
        else if(ui->month->currentText() == j) month = j;
    }

    date = (day + "/" + month + "/" + year);
    name = (name_2 + " " + name_1 + " " + name_3);

    bool flag;
    int x = phone.toInt(&flag, 10);
    int n = 1;
    while ((x/=10) > 0) n++;
    if(n != 9) flag = false;
    if(flag == false){
        QMessageBox::warning(this, "Ошибка", "Введён некоректный номер телефона");
    }

    int id = 0;

    ifstream file("/Users/Asus/Desktop/NECHTO/qt_laba_2/lab2.txt", ios::in);
    string line;

    while(getline(file, line)){
        id++;
    }

    file.close();

    bool check = false;


    if (pass == replay_pass && log.size() != 0 && pass.size() != 0 && name_1.size() != 0 && name_2.size() != 0 && e_mail.size() != 0 && phone.size() != 0)
    {
            check = true;
    }

    for(int i = 0; i < lp.GetVectorSize(); i++){
        if(log == QString::fromStdString(lp.GetData("log", i))){
            QMessageBox::warning(this, "Ошибка", "Такой логин уже существует");
            check = false;
        }
    }

    if (check == true && flag == true){
        DataBaseUser _user;
        _user.DataToFile(name, e_mail, phone, date, "user", log, pass, id);
        this->close();
        QMessageBox::information(this, "Регистрация", "Регистрация прошла успешно.");
        MainWindow *m;
        m = new MainWindow(this);
        m->show();

        //emit QMainWindow();
    }
    else {
        QMessageBox::information(this, "Регистрация", "Вы допустили ошибку при регистрации. Попробуйте ещё раз.");
        ui->lineEdit_3->clear();
        ui->lineEdit_2->clear();
    }

}

void registration::on_pushButton_2_clicked()
{
    this->close();
    MainWindow *wind = new MainWindow();
    wind->show();
}
