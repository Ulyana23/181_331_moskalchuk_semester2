#ifndef WINDOWMENU_H
#define WINDOWMENU_H

#include <QDialog>

namespace Ui {
class WindowMenu;
}

class WindowMenu : public QDialog
{
    Q_OBJECT

public:
    explicit WindowMenu(QWidget *parent = nullptr);
    ~WindowMenu();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();

private:
    Ui::WindowMenu *ui;
};

#endif // WINDOWMENU_H
