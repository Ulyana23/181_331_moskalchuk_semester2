#include "database.h"
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <QString>
#include <vector>

using namespace std;

DataBase::DataBase(){

}

string DataBase::id;

string DataBase::aothorization;

void DataBase::DataToFile(QString name, QString e_mail, QString num, QString date, QString tableName, QString _log, QString _pass, int i){
    _tableName = tableName;
    QFile reg("/Users/Asus/Desktop/NECHTO/qt_laba_2/lab2.txt");
    QFile file("/Users/Asus/Desktop/NECHTO/qt_laba_2/" + _tableName +".txt");
    if (reg.open(QIODevice::Append) && file.open(QIODevice::Append)){

        QTextStream regS(&reg);
        QTextStream fileS(&file);

        regS << "\r\n" << i << ':' << _log << ':' << _pass << ':' << _tableName;
        fileS << "\r\n" << i << ':'  << name << ':' << e_mail << ':' << num << ':' << date;
        file.close();
        reg.close();
    }
}

void DataBase::SetId(int _id){
    id = to_string(_id);
}

string DataBase::GetId(){
    return id;
}

void DataBase::SetAothorization(string ao){
    aothorization = ao;
}

string DataBase::GetAothorization(){
    return aothorization;
}

//MENU//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DataBaseMenu::DataBaseMenu()
{
    FileToVector();
}

vector<DataMenu> DataBaseMenu::VectorMenu;

void DataBaseMenu::FileToVector(){
    ifstream FileMenu("/Users/Asus/Desktop/NECHTO/qt_laba_2/menu.txt", ios::in);
    while(!FileMenu.eof()){
        string str, token;
        getline(FileMenu, str);
        DataMenu item; //объект структуры

        int k = 0;
        istringstream FileMenuS(str);
        while(getline(FileMenuS, token, ':')){
            if(k == 0) item.dish = token; //stoi Извлекает знаковое целое число из строки string
            if(k == 1) item.category = token;
            if(k == 2) item.price = token;
            if(k == 3) item.weight = token;
            k++;
        }
        VectorMenu.push_back(item);
    }
    FileMenu.close();
}

void DataBaseMenu::VectorToFile(){
    QFile FileMenu("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/menu.txt");
    if (FileMenu.open(QIODevice::WriteOnly)){

        QTextStream FileMenuS(&FileMenu);

        FileMenuS << QString::fromLocal8Bit(VectorMenu.at(0).dish.c_str()) << ':' << QString::fromLocal8Bit(VectorMenu.at(0).category.c_str()) << ':' << QString::fromLocal8Bit(VectorMenu.at(0).price.c_str()) << ':' << QString::fromLocal8Bit(VectorMenu.at(0).weight.c_str());
    for (unsigned i = 1; i < (VectorMenu.size()); i++){
        FileMenuS << "\r\n" << QString::fromLocal8Bit(VectorMenu.at(i).dish.c_str()) << ':' << QString::fromLocal8Bit(VectorMenu.at(i).category.c_str()) << ':' << QString::fromLocal8Bit(VectorMenu.at(i).price.c_str()) << ':' << QString::fromLocal8Bit(VectorMenu.at(i).weight.c_str());
    }
    FileMenu.close();
    }
}

void DataBaseMenu::SetMenu(string word, int i, string newitem){
    if(word == "dish")
    VectorMenu.at(i).dish = newitem;

    if(word == "category")
    VectorMenu.at(i).category = newitem;

    if(word == "price")
    VectorMenu.at(i).price = newitem;

    if(word == "weight")
    VectorMenu.at(i).weight = newitem;
}

int DataBaseMenu::GetMenuSize(){

    return VectorMenu.size();
}

string DataBaseMenu::GetMenuElem(string name, int i){
    if(name == "dish")
    return VectorMenu.at(i).dish;

    if(name == "category")
    return VectorMenu.at(i).category;

    if(name == "price")
    return VectorMenu.at(i).price;

    if(name == "weight")
    return VectorMenu.at(i).weight;

}

string DataBaseMenu::Search(string data, string ct, int _i, string name){
    int a = -1;
    for(unsigned i = 0; i < VectorMenu.size(); i++){
        if (ct == "category" && VectorMenu.at(i).category == name){
            a++;
        }
        if (ct == "dish" && VectorMenu.at(i).dish == name){
            a++;
        }
        if (ct == "price" && VectorMenu.at(i).price == name){
            a++;
        }
        if (ct == "weight" && VectorMenu.at(i).weight == name){
            a++;
        }

        if(a == _i){
            if (data == "category"){
                return VectorMenu.at(i).category;
            }
            if (data == "dish"){
                return VectorMenu.at(i).dish;
            }
           if (data == "price"){
                return VectorMenu.at(i).price;
            }
            if (data == "weight"){
                return VectorMenu.at(i).weight;
            }
        }
     }
}

int DataBaseMenu::GetConstSize(string data, string name){
    int a = 0;
    for(unsigned i = 0; i < VectorMenu.size(); i++){
        if (data == "category" && VectorMenu.at(i).category == name){
            a++;
        }
        if (data == "dish" && VectorMenu.at(i).dish == name){
            a++;
        }
        if (data == "price" && VectorMenu.at(i).price == name){
            a++;
        }
        if (data == "weight" && VectorMenu.at(i).weight == name){
            a++;
        }
    }
    return a;
}

//USER//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DataBaseUser::DataBaseUser()
{
    FileToVector();
}

vector<DataPeople> DataBaseUser::VectorUser;

void DataBaseUser::FileToVector(){
    ifstream FileUser("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/user.txt", ios::in);
    while(!FileUser.eof()){
        string str, token;

        getline(FileUser, str); //строка

        DataPeople item; //запись таблицы

        int k = 0;
        istringstream FileUserS(str);
        while(getline(FileUserS, token, ':')){
            if(k == 0) item.id = token; //stoi Извлекает знаковое целое число из строки string
            if(k == 1) item.name = token;
            if(k == 2) item.e_mail = token;
            if(k == 3) item.phone = token;
            if(k == 4) item.date = token;
            k++;
        }

        VectorUser.push_back(item);
    }
    FileUser.close();
}

void DataBaseUser::VectorToFile(){
    ofstream FileUser("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/user.txt", ios::out, ios::trunc);
    for (unsigned i = 0; i < VectorUser.size(); i++){
        FileUser << VectorUser.at(i).id << ':' << VectorUser.at(i).name << ':' << VectorUser.at(i).e_mail << ':' << VectorUser.at(i).phone << ':' << VectorUser.at(i).date << endl;
    }
    FileUser.close();
}


//DataBaseUser DataBaseUser::search(string k, string v){ //тип значения поля - string
//    DataBaseUser select;
//    for (unsigned i = 0; i < VectorUser.size(); i++){
//        DataUser item = VectorUser.at(i);

//        if(k == "name" && item.name == v){
//            select.push_back(item);
//        }

//        if(k == "address" && item.e_mail == v){
//            select.push_back(item);
//        }
//    }
//    return select;
//}


string DataBaseUser::GetData(string data){
        for(unsigned i = 0; i < VectorUser.size(); i++){
            if (data == "name" && VectorUser.at(i).id == GetId())
                return VectorUser.at(i).name;
            if (data == "e_mail" && VectorUser.at(i).id == GetId())
                return VectorUser.at(i).e_mail;
            if (data == "phone" && VectorUser.at(i).id == GetId())
                return VectorUser.at(i).phone;
            if (data == "date" && VectorUser.at(i).id == GetId())
                return VectorUser.at(i).date;
        };
}



//ADMIN//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    DataBaseAdmin::DataBaseAdmin()
{
    FileToVector();
}

vector<DataPeople> DataBaseAdmin::VectorAdmin;

void DataBaseAdmin::FileToVector(){
    ifstream FileAdmin("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/admin.txt", ios::in);
    while(!FileAdmin.eof()){
        string str, token;

        getline(FileAdmin, str); //строка

        DataPeople item; //запись таблицы

        int k = 0;
        istringstream FileAdminS(str);
        while(getline(FileAdminS, token, ':')){
            if(k == 0) item.id = token; //stoi Извлекает знаковое целое число из строки string
            if(k == 1) item.name = token;
            if(k == 2) item.e_mail = token;
            if(k == 3) item.phone = token;
            if(k == 4) item.date = token;
            k++;
        }

        VectorAdmin.push_back(item);
    }
    FileAdmin.close();
}

void DataBaseAdmin::VectorToFile(){
    ofstream FileAdmin("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/admin.txt", ios::out, ios::trunc);
    for (unsigned i = 0; i < VectorAdmin.size(); i++){
        FileAdmin << VectorAdmin.at(i).id << ':' << VectorAdmin.at(i).name << ':' << VectorAdmin.at(i).e_mail << ':' << VectorAdmin.at(i).phone << ':' << VectorAdmin.at(i).date << endl;
    }
    FileAdmin.close();
}


string DataBaseAdmin::GetData(string data){
        for(unsigned i = 0; i < VectorAdmin.size(); i++){
            if (data == "name" && VectorAdmin.at(i).id == GetId())
                return VectorAdmin.at(i).name;
            if (data == "e_mail" && VectorAdmin.at(i).id == GetId())
                return VectorAdmin.at(i).e_mail;
            if (data == "phone" && VectorAdmin.at(i).id == GetId())
                return VectorAdmin.at(i).phone;
            if (data == "date" && VectorAdmin.at(i).id == GetId())
                return VectorAdmin.at(i).date;
        };
}







//WORKER//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    DataBaseWorker::DataBaseWorker(){
    FileToVector();
}

vector<DataPeople> DataBaseWorker::VectorWorker;

void DataBaseWorker::FileToVector(){
    ifstream FileWorker("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/worker.txt", ios::in);
    while(!FileWorker.eof()){
        string str, token;

        getline(FileWorker, str); //строка

        DataPeople item; //запись таблицы

        int k = 0;
        istringstream FileWorkerS(str);
        while(getline(FileWorkerS, token, ':')){
            if(k == 0) item.id = token; //stoi Извлекает знаковое целое число из строки string
            if(k == 1) item.name = token;
            if(k == 2) item.position = token;
            if(k == 3) item.e_mail = token;
            if(k == 4) item.phone = token;
            if(k == 5) item.date = token;
            k++;
        }

        VectorWorker.push_back(item);
    }
    FileWorker.close();
}

void DataBaseWorker::VectorToFile(){

    QFile FileWorker("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/worker.txt");
    if (FileWorker.open(QIODevice::WriteOnly)){

        QTextStream FileWorkerS(&FileWorker);

    for (unsigned i = 0; i < (VectorWorker.size()); i++){
        if(i < (VectorWorker.size() - 1))
        FileWorkerS << QString::fromLocal8Bit(VectorWorker.at(i).id.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).name.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).position.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).e_mail.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).phone.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).date.c_str()) << "\r\n";
        if(i == (VectorWorker.size() - 1))
        FileWorkerS << QString::fromLocal8Bit(VectorWorker.at(i).id.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).name.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).position.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).e_mail.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).phone.c_str()) << ':' << QString::fromLocal8Bit(VectorWorker.at(i).date.c_str());
    }

    FileWorker.close();
    }
}



string DataBaseWorker::GetData(string data){
        for(unsigned i = 0; i < VectorWorker.size(); i++){
            if (data == "name" && VectorWorker.at(i).id == GetId())
                return VectorWorker.at(i).name;
            if (data == "e_mail" && VectorWorker.at(i).id == GetId())
                return VectorWorker.at(i).e_mail;
            if (data == "phone" && VectorWorker.at(i).id == GetId())
                return VectorWorker.at(i).phone;
            if (data == "date" && VectorWorker.at(i).id == GetId())
                return VectorWorker.at(i).date;
            if (data == "position" && VectorWorker.at(i).id == GetId())
                return VectorWorker.at(i).position;
        };
}

int DataBaseWorker::GetVectorSize(){

    return VectorWorker.size();
}

void DataBaseWorker::SetElem(string word, int i, string newitem){
    if(word == "name")
    VectorWorker.at(i).name = newitem;

    if(word == "position")
    VectorWorker.at(i).position = newitem;

    if(word == "e_mail")
    VectorWorker.at(i).e_mail = newitem;

    if(word == "phone")
    VectorWorker.at(i).phone = newitem;

    if(word == "date")
    VectorWorker.at(i).date = newitem;
}

string DataBaseWorker::GetVectorElem(string name, int i){
    if(name == "name")
    return VectorWorker.at(i).name;
    if(name == "date")
    return VectorWorker.at(i).date;
    if(name == "phone")
    return VectorWorker.at(i).phone;
    if(name == "e_mail")
    return VectorWorker.at(i).e_mail;
    if(name == "position")
    return VectorWorker.at(i).position;
}

//LOGPASS///////////////////////////////////////////////////////////////////////////////////////////////


DataBaseLogPass::DataBaseLogPass(){
    FileToVector();
}

vector<DataLogPass> DataBaseLogPass::VectorLogPass;

void DataBaseLogPass::FileToVector(){
ifstream FilelLogPass("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/lab2.txt", ios::in);
while(!FilelLogPass.eof()){
    string str, token;

    getline(FilelLogPass, str); //строка

    DataLogPass item; //запись таблицы

    int k = 0;
    istringstream FilelLogPassS(str);
    while(getline(FilelLogPassS, token, ':')){
        if(k == 0) item.id = token; //stoi Извлекает знаковое целое число из строки string
        if(k == 1) item.log = token;
        if(k == 2) item.pass = token;
        if(k == 3) item.aothorization = token;
        k++;
    }

    VectorLogPass.push_back(item);
}
FilelLogPass.close();
}

void DataBaseLogPass::VectorToFile(){
ofstream FilelLogPass("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/lab2.txt", ios::out, ios::trunc);
for (unsigned i = 0; i < VectorLogPass.size()-1; i++){
    FilelLogPass << VectorLogPass.at(i).id << ':' << VectorLogPass.at(i).log << ':' << VectorLogPass.at(i).pass << ':' << VectorLogPass.at(i).aothorization << endl;
}
FilelLogPass.close();
}

int DataBaseLogPass::GetVectorSize(){
    return VectorLogPass.size();
}

string DataBaseLogPass::GetDataById(string data){
    for(unsigned i = 0; i < VectorLogPass.size(); i++){
        if (data == "log" && VectorLogPass.at(i).id == GetId())
            return VectorLogPass.at(i).log;
        if (data == "pass" && VectorLogPass.at(i).id == GetId())
            return VectorLogPass.at(i).pass;
        if (data == "aothorization" && VectorLogPass.at(i).id == GetId())
            return VectorLogPass.at(i).aothorization;
    };
}

string DataBaseLogPass::GetData(string data, int i){
        if (data == "log")
            return VectorLogPass.at(i).log;
        if (data == "pass")
            return VectorLogPass.at(i).pass;
        if (data == "aothorization")
            return VectorLogPass.at(i).aothorization;
}

void DataBaseLogPass::ChangeData(string namedata, string newdata){
    for(unsigned i = 0; i < VectorLogPass.size(); i++){
        if (namedata == "log" && VectorLogPass.at(i).id == GetId())
            VectorLogPass.at(i).log = newdata;
        if (namedata == "pass" && VectorLogPass.at(i).id == GetId())
            VectorLogPass.at(i).pass = newdata;
    };
}


//JOURNAL////////////////////////////////////////////////////////////////////////////////////////////////////

DataBaseJournal::DataBaseJournal(){
    FileToVector();
}

vector<DataJournal> DataBaseJournal::VectorJournal;

void DataBaseJournal::FileToVector(){
ifstream FilelJournal("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/journal.txt", ios::in);
while(!FilelJournal.eof()){
    string str, token;

    getline(FilelJournal, str); //строка

    DataJournal item; //запись таблицы

    int k = 0;
    istringstream FilelJournalS(str);
    while(getline(FilelJournalS, token, ':')){
        if(k == 0) item.id = token; //stoi Извлекает знаковое целое число из строки string
        if(k == 1) item.time = token;
        if(k == 2) item.date = token;
        if(k == 3) item.name = token;
        if(k == 4) item.dish = token;
        if(k == 5) item.price = token;
        if(k == 6) item.weight = token;
        k++;
    }

    VectorJournal.push_back(item);
}
FilelJournal.close();
}

int DataBaseJournal::GetVectorSize(){

    return VectorJournal.size();
}

int DataBaseJournal::GetConstSize(){
    int a = 0;
    for(unsigned i = 0; i < VectorJournal.size(); i++){
        if (VectorJournal.at(i).id == GetId())
            a++;
    }
    return a;
}

string DataBaseJournal::GetData(string data, int _i){
    int a = -1;
    for(unsigned i = 0; i < VectorJournal.size(); i++){
        if (VectorJournal.at(i).id == GetId()){
            a++;
        }
        if(a == _i){
            if (data == "time"){
                return VectorJournal.at(i).time;
            }
            if (data == "date"){
                return VectorJournal.at(i).date;
            }
            if (data == "name"){
                return VectorJournal.at(i).name;
            }
            if (data == "dish"){
                return VectorJournal.at(i).dish;
            }
            if (data == "price"){
                return VectorJournal.at(i).price;
            }
            if (data == "weight"){
                return VectorJournal.at(i).weight;
            }
        }
     }
}

string DataBaseJournal::GetElem(string name, int i){
    if(name == "name")
    return VectorJournal.at(i).name;
    if(name == "dish")
    return VectorJournal.at(i).dish;
    if(name == "price")
    return VectorJournal.at(i).price;
    if(name == "weight")
    return VectorJournal.at(i).weight;
    if(name == "date")
    return VectorJournal.at(i).date;
    if(name == "time")
    return VectorJournal.at(i).time;

}

void DataBaseJournal::DeleteDish(int _i){
    for(unsigned i = _i; i < VectorJournal.size()-1; i++){
        if(i < VectorJournal.size()){
        VectorJournal.at(i).id = VectorJournal.at(i+1).id;
        VectorJournal.at(i).name = VectorJournal.at(i+1).name;
        VectorJournal.at(i).dish = VectorJournal.at(i+1).dish;
        VectorJournal.at(i).price = VectorJournal.at(i+1).price;
        VectorJournal.at(i).weight = VectorJournal.at(i+1).weight;
        VectorJournal.at(i).time = VectorJournal.at(i+1).time;
        VectorJournal.at(i).date = VectorJournal.at(i+1).date;
        }
    }
}

void DataBaseJournal::VectorClear(){
    VectorJournal.clear();
}

void DataBaseJournal::VectorToFile(){

    QFile FileJournal("C:/Users/Asus/Desktop/NECHTO/qt_laba_2/journal.txt");
    if (FileJournal.open(QIODevice::WriteOnly)){

        QTextStream FileJournalS(&FileJournal);

    for (unsigned i = 0; i < (VectorJournal.size() - 1); i++){
        if(i < (VectorJournal.size() - 2))
        FileJournalS << QString::fromLocal8Bit(VectorJournal.at(i).id.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).time.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).date.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).name.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).dish.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).price.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).weight.c_str()) << "\r\n";
        if(i == (VectorJournal.size() - 2))
        FileJournalS << QString::fromLocal8Bit(VectorJournal.at(i).id.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).time.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).date.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).name.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).dish.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).price.c_str()) << ':' << QString::fromLocal8Bit(VectorJournal.at(i).weight.c_str());
    }

    FileJournal.close();
    }
}
