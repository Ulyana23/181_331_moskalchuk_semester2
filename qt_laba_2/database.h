#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <QString>

#ifndef DATABASE_H
#define DATABASE_H

using namespace std;


struct DataPeople{
    string id; //0
    string phone; //3
    string date; //4
    string name; //1
    string e_mail; //2
    string position; //5
};

struct DataMenu{
    string dish; //блюдо
    string category;
    string price; //цена
    string weight; //вес
};

struct DataLogPass{
    string id;
    string log;
    string pass;
    string aothorization;
};

struct DataJournal{
    string id;
    string time;
    string date;
    string name;
    string dish;
    string price;
    string weight;
};

//_____________________________________________________________

class DataBase{
    static string id;
    static string aothorization;
public:
    DataBase();

    void SetAothorization(string ao);
    string GetAothorization();

    void DataToFile(QString name,QString e_mail, QString num, QString date, QString tableName, QString _log, QString _pass, int i);
    void SetId(int id);
    string GetId();

protected:
    QString _tableName;

};

class DataBaseJournal : public DataBase{
    static vector<DataJournal> VectorJournal;
public:
    DataBaseJournal();

    void VectorClear();
    void FileToVector();
    void VectorToFile();
    string GetData(string data, int _i);
    string GetElem(string name, int i);

    int GetVectorSize();
    int GetConstSize();
    void DeleteDish(int _i);

};

class DataBaseLogPass : public DataBase{
    static vector<DataLogPass> VectorLogPass;
public:
    DataBaseLogPass();

    void VectorToFile();
    string GetDataById(string data);
    string GetData(string data, int i);

    int GetVectorSize();

    void ChangeData(string namedata, string newdata);

private:

    void FileToVector();
};

class DataBaseMenu
{
    static vector<DataMenu> VectorMenu;
public:
    DataBaseMenu();


    int GetMenuSize();
    string GetMenuElem(string name, int i);
    void VectorToFile();
    void SetMenu(string word, int i, string newitem);
    string Search(string word, string ct, int i, string name);
    int GetConstSize(string data, string name);

private:

    void FileToVector();
};

class DataBaseUser : public DataBase
{
    static vector<DataPeople> VectorUser;
public:
    DataBaseUser();



//    void push_back(DataUser item){
//        VectorUser.push_back(item);
//    }

    void VectorToFile();
    string GetData(string data);

    //DataBaseUser search(string k, string v);

private:

    void FileToVector();
};

class DataBaseWorker : public DataBase
{
    static vector<DataPeople> VectorWorker;
public:
    DataBaseWorker();

    void VectorToFile();
    string GetData(string data);

    int GetVectorSize();
    string GetVectorElem(string name, int i);
    void SetElem(string word, int i, string newitem);

private:

    void FileToVector();

};

class DataBaseAdmin : public DataBase
{
    static vector<DataPeople> VectorAdmin;
public:
    DataBaseAdmin();

    void VectorToFile();
    string GetData(string data);

    int GetVectorSize();
    string GetVectorElemName(int i);
    string GetVectorElemPhone(int i);
    string GetVectorElemE_mail(int i);
    string GetVectorElemPosition(int i);
    string GetVectorElemDate(int i);

private:

    void FileToVector();

};

#endif // DATABASE_H
