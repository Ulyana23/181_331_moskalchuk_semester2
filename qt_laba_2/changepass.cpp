#include "changepass.h"
#include "ui_changepass.h"
#include "database.h"
#include "windowpersonalarea.h"
#include "workerpersonalarea.h"
#include <QMessageBox>

ChangePass::ChangePass(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangePass)
{
    ui->setupUi(this);
    setWindowTitle("Смена логина и пароля");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
}

ChangePass::~ChangePass()
{
    delete ui;
}

void ChangePass::on_pushButton_2_clicked()
{
    this->close();
    DataBase person;
    if (person.GetAothorization() == "admin" || person.GetAothorization() == "user"){
        WindowPersonalArea *wpa = new WindowPersonalArea(this);
        wpa->show();

    }
    if (person.GetAothorization() == "worker"){
        WorkerPersonalArea *pa = new WorkerPersonalArea(this);
        pa->show();

    }


}

void ChangePass::on_pushButton_clicked()
{
    QString _oldpass = ui->oldpass->text();
    QString _newpass_1 = ui->newpass_1->text();
    QString _newpass_2 = ui->newpass_2->text();
    QString _newlog = ui->newlog->text();

    string oldpass = _oldpass.toUtf8().constData();
    string newpass_1 = _newpass_2.toUtf8().constData();
    string newpass_2 = _newpass_2.toUtf8().constData();
    string newlog = _newlog.toUtf8().constData();

    DataBaseLogPass pass;
    if ((pass.GetDataById("pass") == oldpass && newpass_1 == newpass_2 && newpass_1.size() != 0) || newlog.size() !=0){
        if(pass.GetDataById("pass") == oldpass && newpass_1 == newpass_2 && newpass_1.size() != 0){
            pass.ChangeData("pass", newpass_1);
            QMessageBox::information(this,"Смена пароля","Поздравляем! Вы успешно сменили ваш пароль.");
        }
        if (newlog.size() != 0){
            pass.ChangeData("log", newlog);
            QMessageBox::information(this,"Смена логина","Поздравляем! Вы успешно сменили ваш логин.");
        }
        pass.VectorToFile();
        this->close();
        DataBase person;
        if (person.GetAothorization() == "admin" || person.GetAothorization() == "user"){
            WindowPersonalArea *wpa = new WindowPersonalArea(this);
            wpa->show();

        }
        if (person.GetAothorization() == "worker"){
            WorkerPersonalArea *pa = new WorkerPersonalArea(this);
            pa->show();

        }
    }

    else{
        QMessageBox::warning(this,"Ошибка","Вы совершили ошибку. Попробуйте ещё раз.");
    }


}
