#include "windowshowworker.h"
#include "ui_windowshowworker.h"
#include "windowadmin.h"
#include "database.h"
#include "windowsaveworker.h"
#include <QMessageBox>

WindowShowWorker::WindowShowWorker(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowShowWorker)
{
    ui->setupUi(this);
    setWindowTitle("Список Сотрудников");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);

    DataBaseWorker w;
    //ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//отмена редактирования ВСЕХ полей
    ui->tableWidget->setRowCount(w.GetVectorSize());
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Имя" << "Должность" << "Почта" << "Номер телефона" << "Дата рождения");

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);

    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        QTableWidgetItem *checkBoxItem = new QTableWidgetItem();
        checkBoxItem->setCheckState(Qt::Unchecked);

        QTableWidgetItem *itm = new QTableWidgetItem(QString::fromLocal8Bit(w.GetVectorElem("name", i).c_str()));
        QTableWidgetItem *itm1 = new QTableWidgetItem(QString::fromLocal8Bit(w.GetVectorElem("position", i).c_str()));
        QTableWidgetItem *itm2 = new QTableWidgetItem(QString::fromLocal8Bit(w.GetVectorElem("e_mail", i).c_str()));
        QTableWidgetItem *itm3 = new QTableWidgetItem(QString::fromLocal8Bit(w.GetVectorElem("phone", i).c_str()));
        QTableWidgetItem *itm4 = new QTableWidgetItem(QString::fromLocal8Bit(w.GetVectorElem("date", i).c_str()));

            ui->tableWidget->setItem(i, 0, itm);
            ui->tableWidget->setItem(i, 1, itm1);
            ui->tableWidget->setItem(i, 2, itm2);
            ui->tableWidget->setItem(i, 3, itm3);
            ui->tableWidget->setItem(i, 4, itm4);

        }
}

WindowShowWorker::~WindowShowWorker()
{
    delete ui;
}

void WindowShowWorker::on_pushButton_2_clicked()
{
    this->close();
    WindowAdmin *w = new WindowAdmin(this);
    w->show();
}

void WindowShowWorker::on_pushButton_clicked()
{
    this->close();
    WindowSaveWorker *save = new WindowSaveWorker(this);
    save->show();
}

void WindowShowWorker::on_pushButton_3_clicked()
{
    bool flag = false;

    string Name;
    string Position;
    string E_mail;
    string Phone;
    string Date;
    DataBaseWorker w;

    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {

        Name = ui->tableWidget->item(i, 0)->text().toLocal8Bit().constData();
        Position = ui->tableWidget->item(i, 1)->text().toLocal8Bit().constData();
        E_mail = ui->tableWidget->item(i, 2)->text().toLocal8Bit().constData();
        Phone = ui->tableWidget->item(i, 3)->text().toLocal8Bit().constData();
        Date = ui->tableWidget->item(i, 4)->text().toLocal8Bit().constData();

        if(w.GetVectorElem("name", i) != Name || w.GetVectorElem("position", i) != Position || w.GetVectorElem("e_mail", i) != E_mail || w.GetVectorElem("phone", i) != Phone || w.GetVectorElem("date", i) != Date){
            w.SetElem("name", i, Name);
            w.SetElem("position", i, Position);
            w.SetElem("e_mail", i, E_mail);
            w.SetElem("phone", i, Phone);
            w.SetElem("date", i, Date);
            flag = true;
        }
    }
    if(flag == true){
        w.VectorToFile();
        QMessageBox::information(this, "Сохранение", "Изменения успешно сохранены");
    }
    else QMessageBox::warning(this, "Ошибка", "Вы не произвели никаких изменений");
}
