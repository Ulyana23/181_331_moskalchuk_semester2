#include "workerpersonalarea.h"
#include "ui_workerpersonalarea.h"
#include "windowworker.h"
#include "database.h"
#include "changepass.h"

WorkerPersonalArea::WorkerPersonalArea(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WorkerPersonalArea)
{
    ui->setupUi(this);
    setWindowTitle("Личный Кабинет");

    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);

    DataBaseWorker w;
    DataBaseLogPass lp;
    ui->name->setText(QString::fromLocal8Bit(w.GetData("name").c_str()));
    ui->e_mail->setText(QString::fromLocal8Bit(w.GetData("e_mail").c_str()));
    ui->phone->setText(QString::fromLocal8Bit(w.GetData("phone").c_str()));
    ui->date->setText(QString::fromLocal8Bit(w.GetData("date").c_str()));
    ui->position->setText(QString::fromLocal8Bit(w.GetData("position").c_str()));
    ui->log->setText(QString::fromLocal8Bit(lp.GetDataById("log").c_str()));
}

WorkerPersonalArea::~WorkerPersonalArea()
{
    delete ui;
}

void WorkerPersonalArea::on_pushButton_2_clicked()
{
    this->close();
    WindowWorker *w = new WindowWorker(this);
    w->show();
}

void WorkerPersonalArea::on_pushButton_clicked()
{
    this->close();
    ChangePass *chp = new ChangePass(this);
    chp->show();
}
