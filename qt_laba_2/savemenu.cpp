#include "savemenu.h"
#include "ui_savemenu.h"
#include "adminmenu.h"
#include <QFile>
#include <QTextStream>
#include "adminmenu.h"

SaveMenu::SaveMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveMenu)
{
    ui->setupUi(this);
    setWindowTitle("Добавить блюдо");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
}

SaveMenu::~SaveMenu()
{
    delete ui;
}

void SaveMenu::on_pushButton_clicked()
{
    this->close();
    AdminMenu *am = new AdminMenu(this);
    am->show();
}

void SaveMenu::on_pushButton_2_clicked()
{
    QString dish = ui->dish->text();
    QString category = ui->category->text();
    QString price = ui->price->text();
    QString weight = ui->weight->text();

    QFile menu("/Users/Asus/Desktop/NECHTO/qt_laba_2/menu.txt");

    if (menu.open(QIODevice::Append)){
        QTextStream menuS(&menu);
        menuS << "\r\n" << dish << ':' << category << ':' << price << ':' << weight;
        menu.close();
}
    this->close();
    AdminMenu *am = new AdminMenu(this);
    am->show();
}
