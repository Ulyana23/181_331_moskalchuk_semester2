#ifndef WINDOWWORKER_H
#define WINDOWWORKER_H

#include <QDialog>

namespace Ui {
class WindowWorker;
}

class WindowWorker : public QDialog
{
    Q_OBJECT

public:
    explicit WindowWorker(QWidget *parent = nullptr);
    ~WindowWorker();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::WindowWorker *ui;
};

#endif // WINDOWWORKER_H
