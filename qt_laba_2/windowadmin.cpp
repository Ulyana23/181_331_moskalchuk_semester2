#include "windowadmin.h"
#include "ui_windowadmin.h"
#include "windowshowworker.h"
#include "database.h"
#include "windowpersonalarea.h"
#include "adminmenu.h"
#include "mainwindow.h"
#include "workerjournal.h"

WindowAdmin::WindowAdmin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowAdmin)
{
    ui->setupUi(this);
    setWindowTitle("Главное Меню");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);
}

WindowAdmin::~WindowAdmin()
{
    delete ui;
}

void WindowAdmin::on_pushButton_clicked()
{
    this->close();
    WindowShowWorker *wshw = new WindowShowWorker(this);
    wshw->show();

}

void WindowAdmin::on_pushButton_4_clicked()
{
    this->close();
    WindowPersonalArea *pa = new WindowPersonalArea(this);
    pa->show();
}

void WindowAdmin::on_pushButton_2_clicked()
{
    this->close();
    AdminMenu *am = new AdminMenu(this);
    am->show();
}

void WindowAdmin::on_pushButton_3_clicked()
{
    this->close();
    WorkerJournal *j = new WorkerJournal(this);
    j->show();
}

void WindowAdmin::on_pushButton_5_clicked()
{
    this->close();
    MainWindow *w = new MainWindow(this);
    w->show();
}
