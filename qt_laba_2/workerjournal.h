#ifndef WORKERJOURNAL_H
#define WORKERJOURNAL_H

#include <QDialog>

namespace Ui {
class WorkerJournal;
}

class WorkerJournal : public QDialog
{
    Q_OBJECT

public:
    explicit WorkerJournal(QWidget *parent = nullptr);
    ~WorkerJournal();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::WorkerJournal *ui;
};

#endif // WORKERJOURNAL_H
