#include "windowpersonalarea.h"
#include "ui_windowpersonalarea.h"
#include "windowadmin.h"
#include "windowuser.h"
#include "database.h"
#include "changepass.h"

WindowPersonalArea::WindowPersonalArea(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowPersonalArea)
{
    ui->setupUi(this);
    setWindowTitle("Личный Кабинет");
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor("#fae0fc")); // устанавливаем цвет фона
    setAutoFillBackground(true);
    setPalette(Pal);

    DataBase person;
    if (person.GetAothorization() == "admin"){
        DataBaseAdmin a;
        DataBaseLogPass lp;
        ui->log->setText(QString::fromLocal8Bit(lp.GetDataById("log").c_str()));
        ui->name->setText(QString::fromLocal8Bit(a.GetData("name").c_str()));
        ui->e_mail->setText(QString::fromLocal8Bit(a.GetData("e_mail").c_str()));
        ui->phone->setText(QString::fromLocal8Bit(a.GetData("phone").c_str()));
        ui->date->setText(QString::fromLocal8Bit(a.GetData("date").c_str()));
    }
    if (person.GetAothorization() == "user"){
        DataBaseUser u;
        DataBaseLogPass lp;
        ui->log->setText(QString::fromLocal8Bit(lp.GetDataById("log").c_str()));
        ui->name->setText(QString::fromLocal8Bit(u.GetData("name").c_str()));
        ui->e_mail->setText(QString::fromLocal8Bit(u.GetData("e_mail").c_str()));
        ui->phone->setText(QString::fromLocal8Bit(u.GetData("phone").c_str()));
        ui->date->setText(QString::fromLocal8Bit(u.GetData("date").c_str()));
    }
}

WindowPersonalArea::~WindowPersonalArea()
{
    delete ui;
}

void WindowPersonalArea::on_pushButton_2_clicked()
{
    this->close();
    DataBase person;
    if (person.GetAothorization() == "admin"){
        WindowAdmin *a = new WindowAdmin(this);
        a->show();
    }
    if (person.GetAothorization() == "user"){
        WindowUser *u = new WindowUser(this);
        u->show();
    }


}

void WindowPersonalArea::on_pushButton_clicked()
{
    this->close();
    ChangePass *chp = new ChangePass(this);
    chp->show();
}
