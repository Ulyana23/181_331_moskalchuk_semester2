#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class Server : public QObject
{
    Q_OBJECT
public:
    Server();

public slots:
    void slotnewConnection();
    void readClient();
    void slotClientDisconnected();

private:
    QTcpServer *_server;
    QTcpSocket *_socket;

};

#endif // SERVER_H
