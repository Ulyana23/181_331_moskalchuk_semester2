#include <QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlRecord>

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    db.setDatabaseName("C:\\Users\\Asus\\Desktop\\NECHTO\\lab_5_2\\progress.txt");
    db.setDatabaseName("C:\\Users\\Asus\\Desktop\\NECHTO\\lab_5_2\\students.txt");


    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError().text();
        return false;
    }
    return true;
}
int main(int argc, char *argv[])
{
     setlocale(LC_ALL, "Russian");
    QCoreApplication a(argc, argv);
    if (!createConnection()) {
        return -1;
    }
    // Создаем базу
    QSqlQuery query;
    QString str = "CREATE TABLE students (record_book AUTO_INCREMENT, name VARCHAR(15), psp_ser AUTO_INCREMENT, psp_num AUTO_INCREMENT);";
    if (!query.exec(str)) {
     qDebug() << "Unable to create a table";
     }

    // Добавляем данные в базу
    QString strF =
            "INSERT INTO students (record_book, name, psp_ser, psp_num) "
            "VALUES(%1, '%2', %3, %4);";
    str = strF.arg("55500")
            .arg("Иванов Иван Петрович")
            .arg("0402")
            .arg("645327");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("55800")
            .arg("Климов Андрей Иванович")
            .arg("0402")
            .arg("673211");

    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("55865")
            .arg("Новиков Николай Юрьевич")
            .arg("0202")
            .arg("554390");

    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    if (!query.exec("SELECT * FROM students;")) {
        qDebug() << "Unable to execute query — exiting";
        return 1;
    }
    // Считываем данные из базы
    QSqlRecord rec = query.record();
    int nNumber = 0;
    QString strName;
    QString strPhone;
    QString strEmail;
    while (query.next()) {
        nNumber = query.value(rec.indexOf("record_book")).toInt();
        strName = query.value(rec.indexOf("name")).toString();
        strPhone = query.value(rec.indexOf("psp_ser")).toString();
        strEmail = query.value(rec.indexOf("psp_num")).toString();
        qDebug() << nNumber << " " << strName << ";\t"
                 << strPhone << ";\t" << strEmail;
    }
    QSqlQuery query1;
    str = "CREATE TABLE progress (record_book AUTO_INCREMENT, discipline VARCHAR(15), acad_year VARCHAR(15), term AUTO_INCREMENT, mark AUTO_INCREMENT);";
    if (!query1.exec(str)) {
     qDebug() << "Unable to create a table";
     }

    // Добавляем данные в базу
    strF =
            "INSERT INTO progress (record_book, discipline, acad_year, term, mark) "
            "VALUES(%1, '%2', '%3', %4, %5);";
    str = strF.arg("55500")
            .arg("Физика")
            .arg("2017/2018")
            .arg("1")
            .arg("5");
    if (!query1.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("55500")
            .arg("Математика")
            .arg("2017/2018")
            .arg("1")
            .arg("4");
    if (!query1.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("55800")
            .arg("Физика")
            .arg("2017/2018")
            .arg("1")
            .arg("4");
    if (!query1.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    str = strF.arg("55800")
            .arg("Физика")
            .arg("2017/2018")
            .arg("2")
            .arg("5");
    if (!query1.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }

    if (!query1.exec("SELECT * FROM progress;")) {
        qDebug() << "Unable to execute query — exiting";
        return 1;
    }
    // Считываем данные из базы
    rec = query1.record();
    nNumber = 0;
    QString strEmail1;
    qDebug() << "\n\r";
    while (query1.next()) {
        nNumber = query1.value(rec.indexOf("record_book")).toInt();
        strName = query1.value(rec.indexOf("discipline")).toString();
        strPhone = query1.value(rec.indexOf("acad_year")).toString();
        strEmail = query1.value(rec.indexOf("term")).toString();
        strEmail1 = query1.value(rec.indexOf("mark")).toString();
        qDebug() << nNumber << " " << strName << ";\t"
                 << strPhone << ";\t" << strEmail << ";\t" << strEmail1;
    }

    str = "SELECT COUNT(*) FROM students JOIN progress ON students.record_book = progress.record_book WHERE mark LIKE '5' AND discipline LIKE 'Физика';";
    if (!query.exec(str)) {
    qDebug() << "Unable to make insert operation";
    }
    query.first();

    qDebug() << query.value(0).toInt();
    qDebug() <<

    query.exec("drop table students");
    query1.exec("drop table progress");
    return 0;

    //return a.exec();
}
