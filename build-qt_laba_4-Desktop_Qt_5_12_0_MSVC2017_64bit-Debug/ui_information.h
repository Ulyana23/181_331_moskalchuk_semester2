/********************************************************************************
** Form generated from reading UI file 'information.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFORMATION_H
#define UI_INFORMATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_Information
{
public:
    QGridLayout *gridLayout;
    QFrame *line_3;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *log;
    QLabel *id;
    QLabel *label;
    QFrame *line_2;
    QLabel *username;
    QLabel *pass;
    QFrame *line;
    QLabel *label_4;

    void setupUi(QDialog *Information)
    {
        if (Information->objectName().isEmpty())
            Information->setObjectName(QString::fromUtf8("Information"));
        Information->resize(322, 300);
        gridLayout = new QGridLayout(Information);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        line_3 = new QFrame(Information);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_3, 1, 0, 1, 2);

        label_2 = new QLabel(Information);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        label_3 = new QLabel(Information);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 4, 0, 1, 1);

        log = new QLabel(Information);
        log->setObjectName(QString::fromUtf8("log"));

        gridLayout->addWidget(log, 6, 1, 1, 1);

        id = new QLabel(Information);
        id->setObjectName(QString::fromUtf8("id"));

        gridLayout->addWidget(id, 4, 1, 1, 1);

        label = new QLabel(Information);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        line_2 = new QFrame(Information);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 3, 0, 1, 2);

        username = new QLabel(Information);
        username->setObjectName(QString::fromUtf8("username"));

        gridLayout->addWidget(username, 0, 1, 1, 1);

        pass = new QLabel(Information);
        pass->setObjectName(QString::fromUtf8("pass"));

        gridLayout->addWidget(pass, 2, 1, 1, 1);

        line = new QFrame(Information);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 5, 0, 1, 2);

        label_4 = new QLabel(Information);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 6, 0, 1, 1);


        retranslateUi(Information);

        QMetaObject::connectSlotsByName(Information);
    } // setupUi

    void retranslateUi(QDialog *Information)
    {
        Information->setWindowTitle(QApplication::translate("Information", "Dialog", nullptr));
        label_2->setText(QApplication::translate("Information", "\320\237\320\260\321\200\320\276\320\273\321\214:", nullptr));
        label_3->setText(QApplication::translate("Information", "ID:", nullptr));
        log->setText(QApplication::translate("Information", "TextLabel", nullptr));
        id->setText(QApplication::translate("Information", "TextLabel", nullptr));
        label->setText(QApplication::translate("Information", "\320\233\320\276\320\263\320\270\320\275:", nullptr));
        username->setText(QApplication::translate("Information", "TextLabel", nullptr));
        pass->setText(QApplication::translate("Information", "TextLabel", nullptr));
        label_4->setText(QApplication::translate("Information", "\320\222\321\213:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Information: public Ui_Information {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFORMATION_H
