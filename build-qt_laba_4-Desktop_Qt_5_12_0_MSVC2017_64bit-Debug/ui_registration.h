/********************************************************************************
** Form generated from reading UI file 'registration.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTRATION_H
#define UI_REGISTRATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_registration
{
public:
    QWidget *widget;
    QGridLayout *gridLayout;
    QRadioButton *admin;
    QLineEdit *login;
    QLineEdit *password;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *password_two;
    QLabel *label_3;
    QRadioButton *user;
    QPushButton *registration_2;

    void setupUi(QDialog *registration)
    {
        if (registration->objectName().isEmpty())
            registration->setObjectName(QString::fromUtf8("registration"));
        registration->resize(400, 300);
        widget = new QWidget(registration);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(20, 10, 361, 281));
        gridLayout = new QGridLayout(widget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        admin = new QRadioButton(widget);
        admin->setObjectName(QString::fromUtf8("admin"));

        gridLayout->addWidget(admin, 3, 0, 1, 1);

        login = new QLineEdit(widget);
        login->setObjectName(QString::fromUtf8("login"));

        gridLayout->addWidget(login, 0, 1, 1, 1);

        password = new QLineEdit(widget);
        password->setObjectName(QString::fromUtf8("password"));

        gridLayout->addWidget(password, 1, 1, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        password_two = new QLineEdit(widget);
        password_two->setObjectName(QString::fromUtf8("password_two"));

        gridLayout->addWidget(password_two, 2, 1, 1, 1);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        user = new QRadioButton(widget);
        user->setObjectName(QString::fromUtf8("user"));

        gridLayout->addWidget(user, 3, 1, 1, 1);

        registration_2 = new QPushButton(widget);
        registration_2->setObjectName(QString::fromUtf8("registration_2"));

        gridLayout->addWidget(registration_2, 4, 0, 1, 2);


        retranslateUi(registration);

        QMetaObject::connectSlotsByName(registration);
    } // setupUi

    void retranslateUi(QDialog *registration)
    {
        registration->setWindowTitle(QApplication::translate("registration", "Dialog", nullptr));
        admin->setText(QApplication::translate("registration", "\320\220\320\264\320\274\320\270\320\275\320\270\321\201\321\202\321\200\320\260\321\202\320\276\321\200", nullptr));
        label->setText(QApplication::translate("registration", "\320\233\320\276\320\263\320\270\320\275", nullptr));
        label_2->setText(QApplication::translate("registration", "\320\237\320\260\321\200\320\276\320\273\321\214", nullptr));
        label_3->setText(QApplication::translate("registration", "\320\237\320\276\320\262\321\202\320\276\321\200\320\270\321\202\320\265 \320\277\320\260\321\200\320\276\320\273\321\214", nullptr));
        user->setText(QApplication::translate("registration", "\320\237\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\214", nullptr));
        registration_2->setText(QApplication::translate("registration", "\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217", nullptr));
    } // retranslateUi

};

namespace Ui {
    class registration: public Ui_registration {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTRATION_H
